#include "PointerFun.h"

//1
int *address;

//2
float balance;
float *temp = &balance;

//3
char letter = 'W';
char *letter_ptr;
char *letter_ptr = &letter;

//4
void output_4() {
    int count = 10, *temp, sum = 0;
    temp = &count;
    *temp = 20;
    temp = &sum;
    *temp = count;
    printf("count = %d, *temp = %d, sum = %d\n", count, *temp, sum);
}

//5
char *message = "Hello";

//6
void output_6() {
    printf("Fairly certain the code in problem 6 is C++ code.  Regardless 9772, 6940, then 'S' ?\n");
}

//7
void output_7(){
    printf("The variable *p will change variable i to 75.");
}

//8
void output_8() {
    printf("The pointer variable double *p cannot hold the type char.\n");
}

//9
void output_9() {

    char blocks[3] = {'A', 'B', 'C'};
    char *ptr = &blocks[0];
    char temp;

    temp = blocks[0];
    printf("%c\n", temp);
    temp = *(blocks + 2);
    printf("%c\n", temp);
    temp = *(ptr + 1);
    printf("%c\n", temp);
    temp = *ptr;
    printf("%c\n", temp);

    ptr = blocks + 1;
    printf("%c\n", temp);
    temp = *ptr;
    printf("%c\n", temp);
    temp = *(ptr + 1);
    printf("%c\n", temp);

    ptr = blocks;
    printf("%c\n", temp);
    temp = *++ptr;
    printf("%c\n", temp);
    temp = ++*ptr;
    printf("%c\n", temp);
    temp = *ptr++;
    printf("%c\n", temp);
    temp = *ptr;
    printf("%c\n", temp);

    return;

}

//10
void reverse(char *string)
{
    int length, c;
    char *begin, *end, temp;

    length = string_length(string);

    begin = string;
    end = string;

    for ( c = 0 ; c < ( length - 1 ) ; c++ )
        end++;

    for ( c = 0 ; c < length/2 ; c++ )
    {
        temp = *end;
        *end = *begin;
        *begin = temp;

        begin++;
        end--;
    }
}

//10 / 13 / 15
void output_10(){
    char s[10] = "abcde";

    reverse(s);

    printf("Reverse of entered string is \"%s\".\n", s);

    return;
}

int string_length(char *pointer)
{
    int c = 0;

    while( *(pointer+c) != '\0' )
        c++;

    return c;
}

//11

void output_11(){
    int array[] = {1,2,3,4,5,6,7,8,9,10};
    int num_even = countEven(array, sizeof(array)/sizeof(int));
    printf("Total Even Numbers: %d\n", num_even);
}

int countEven(int* num_array, int size){
    int num_even = 0;
    int check_num;
    int i;

    for(i = 0; i != size; i++){
        check_num = *(num_array + i);
        if(check_num % 2 == 0){
            num_even++;
        }
    }
    return num_even;
}

//12

void output_12(){
    double array[] = {2, 4, 6, 8, 12};
    double* returned_pointer = max_double_pointer(array, sizeof(array)/sizeof(int));
    printf("Pointer Address: %p", returned_pointer);
}

double* max_double_pointer(double test_array[], int size){
    double* biggest_num = test_array;

    for(int i = 0; i != size; i++){
        if(*(test_array + i) < *(test_array + i + 1)){
            *biggest_num = *(test_array+1);
        }
        else {
            *biggest_num = *test_array;
        }
    }

    return (biggest_num);
}

//Output
void PointerFun_Run() {
    printf("Output 4: \n");
    output_4();
    printf("Output 6: \n");
    output_6();
    printf("Output 7: \n");
    output_7();
    printf("Output 8: \n");
    output_8();
    printf("Output 9: \n");
    output_9();
    printf("Output 10: \n");
    output_10();
    printf("Output 11: \n");
    output_11();
    printf("Output 12: \n");
    output_12();
}