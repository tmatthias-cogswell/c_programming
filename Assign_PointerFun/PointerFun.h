#ifndef C_PROGRAMMING_POINTERFUN_H
#define C_PROGRAMMING_POINTERFUN_H

#include <stdio.h>

int *address;
float balance;
float *temp;
char letter;
char *letter_ptr;
char *message;

void PointerFun_Run();
void output_4();
void output_6();
void output_7();
void output_8();
void output_9();
void output_10();
void output_11();
void output_12();
void reverse(char *string);
int string_length(char *pointer);
int countEven(int*, int);
double* max_double_pointer(double*, int);


#endif
