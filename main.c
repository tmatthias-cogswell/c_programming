#include "Assign_FiveFunc/FiveFunc.h"
#include "Assign_PointerFun/PointerFun.h"
#include "Assign_StringManip/StringManip.h"
#include "Assign_FOPEN/learn_fopen.h"
#include "Assign_Bitwise/Bitwise.h"
#include "Assign_Struct_Enum/StructEnum_Test.h"
#include "Assign_Stacks/my_queue.h"
#include "Assign_LinkedList/my_linkedList.h"

int main() {
    /* Submitted Assignments Disabled:
    Assign_FiveFunc();
    PointerFun_Run();
    StringManip_All();
    File_IO_Run();
    Run_Bit_Manip();
    student_test_run();
    lemTest();
    run_q_test();
    */

    run_lL_test();
}