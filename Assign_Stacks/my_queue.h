#ifndef C_PROGRAMMING_MY_QUEUE_H
#define C_PROGRAMMING_MY_QUEUE_H

typedef struct Node {
    int data;
    struct Node *next;
} intNode;

typedef struct MyQueue {
    intNode *start;
    intNode *end;
    int count;

} intQueue;

void push_to_q(intQueue *, int);

void pop_from_q(intQueue *);

void print_q(intQueue *);

intNode* find_in_q(intQueue *, int);

void run_q_test();

#endif //C_PROGRAMMING_MY_QUEUE_H
