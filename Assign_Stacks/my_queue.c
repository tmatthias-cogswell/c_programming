#include "my_queue.h"
#include <stdlib.h>
#include <stdio.h>

void push_to_q(intQueue *in_q, int in_data){

    if (in_q->count == 0 ){
        intNode *add = malloc(sizeof(intNode));
        add->data = in_data;
        add->next = NULL;
        in_q->start = in_q->end = add;
        in_q->count++;
    }

    else if (in_q->count > 0) {
        intNode *add = malloc(sizeof(intNode));
        add->data = in_data;
        add->next = in_q->start;
        in_q->start = add;
        in_q->count++;
    }
}

void pop_from_q(intQueue *in_q){
    free(in_q->end);
    in_q->count--;

    intNode *iter = in_q->start;

    for (int i = 1; i <= in_q->count; i++){
        if (i == in_q->count){
            in_q->end = iter;
            iter->next = NULL;
        }

        else {
            iter = iter->next;
        }
    }
}

void print_q(intQueue *in_q){
    intNode *temp_intNode = in_q->start;
    for(int i = 1; i <= in_q->count; i++){
        printf("\nValue %i: %i", i, temp_intNode->data);
        temp_intNode = temp_intNode->next;
    }
}

intNode* find_in_q(intQueue *in_q, int in_data){
    intNode *temp_intNode = in_q->start;
    int position = 0;
    for(int i = 1; i <= in_q->count; i++){
        if (temp_intNode->data == in_data){
            return temp_intNode;
        }
        temp_intNode = temp_intNode->next;
    }
    return NULL;
}

void run_q_test(){
    intQueue test_q;

    test_q.count = 0;
    for (int i = 1; i <= 10; i++){
        push_to_q(&test_q, (i * 16));
    }

    printf("\nAdding Values via FILO: ");
    print_q(&test_q);
    printf("\nRemoving via FILO: ");
    pop_from_q(&test_q);
    print_q(&test_q);

    intNode *test_find = find_in_q(&test_q, 96);
    printf("\nPointer to Value 96: %p", test_find);
    printf("\nPointer Value: %i", test_find->data);
}