#include "my_linkedList.h"
#include <stdlib.h>
#include <stdio.h>

int_lLNode *push_to_back(intLinkedList *in_lL, int in_data) {

    if (in_lL->count == 0) {
        int_lLNode *add = malloc(sizeof(int_lLNode));
        add->data = in_data;
        add->next = NULL;
        in_lL->start = in_lL->end = add;
        in_lL->count++;
    }

    else if (in_lL->count > 0) {
        int_lLNode *add = malloc(sizeof(int_lLNode));
        add->data = in_data;
        add->next = in_lL->start;
        in_lL->end->next = add;
        in_lL->end = add;
        in_lL->count++;
    }
    return in_lL->end;
}

int_lLNode *delete_from_lL(intLinkedList *in_lL, int_lLNode *pelement) {

    int_lLNode *iter = in_lL->start;

    for (int i = 1; i <= in_lL->count; i++) {
        if (in_lL->start == iter && iter->data == pelement->data) {
            int_lLNode *temp = iter->next;
            free(iter);
            in_lL->start = temp;
            in_lL->count--;
            return in_lL->start;
        }

        else if (iter->next == in_lL->end && iter->next->data == pelement->data) {
            free(iter->next);
            in_lL->end = iter;
            iter->next->next = NULL;
            in_lL->count--;
            return in_lL->start;
        }

        else if (iter->next->data == pelement->data) {
            int_lLNode *temp = iter->next->next;
            free(iter->next);
            iter->next = temp;
            in_lL->count--;
            return in_lL->start;
        }
        else {
            iter = iter->next;
        }
    }
    return in_lL->start;
}

void print_lL(intLinkedList *in_lL) {
    int_lLNode *temp_int_lLNode = in_lL->start;
    for (int i = 1; i <= in_lL->count; i++) {
        printf("\nValue %i - %p: %i", i, temp_int_lLNode, temp_int_lLNode->data);
        temp_int_lLNode = temp_int_lLNode->next;
    }
}

int_lLNode *find_in_lL(intLinkedList *in_lL, int in_data) {
    int_lLNode *temp_int_lLNode = in_lL->start;
    for (int i = 1; i <= in_lL->count; i++) {
        if (temp_int_lLNode->data == in_data) {
            return temp_int_lLNode;
        }
        temp_int_lLNode = temp_int_lLNode->next;
    }
    return NULL;
}

void free_lList(intLinkedList *in_lL) {
    int_lLNode *iter = in_lL->start;

    for (int i = 1; i <= in_lL->count; i++) {
        if (iter == in_lL->end) {
            free(iter);
        }

        else {
            int_lLNode *temp = iter->next;
            free(iter);
            iter = temp;
        }
    }
}

void run_lL_test() {
    intLinkedList test_lL;

    test_lL.count = 0;
    for (int i = 1; i <= 10; i++) {
        push_to_back(&test_lL, (i * 16));
    }

    printf("\nAdding Values: ");
    print_lL(&test_lL);

    int_lLNode *test_find = find_in_lL(&test_lL, 96);
    printf("\nPointer to Value 96: %p", test_find);
    printf("\nPointer Value: %i", test_find->data);

    printf("\nRemoving first value: ");
    delete_from_lL(&test_lL, find_in_lL(&test_lL, 16));
    print_lL(&test_lL);

    printf("\nRemoving last value: ");
    delete_from_lL(&test_lL, find_in_lL(&test_lL, 160));
    print_lL(&test_lL);

    printf("\nRemoving specific value (80): ");
    delete_from_lL(&test_lL, find_in_lL(&test_lL, 80));
    print_lL(&test_lL);

    printf("\nDeleting List...");
    free_lList(&test_lL);
}
