#ifndef C_PROGRAMMING_MY_LINKEDLIST_H
#define C_PROGRAMMING_MY_LINKEDLIST_H

typedef struct lLNode {
    int data;
    struct lLNode *next;
} int_lLNode;

typedef struct MyLinkedList {
    int_lLNode *start;
    int_lLNode *end;
    int count;

} intLinkedList;

int_lLNode *push_to_back(intLinkedList *, int);

int_lLNode *delete_from_lL(intLinkedList *, int_lLNode *);

void print_lL(intLinkedList *);

int_lLNode* find_in_lL(intLinkedList *, int);

void free_lList(intLinkedList *);

void run_lL_test();

#endif //C_PROGRAMMING_MY_LINKEDLIST_H
