#ifndef C_PROGRAMMING_BITWISE_H
#define C_PROGRAMMING_BITWISE_H

int Get_Dec(char *);
int Get_Bin(int);
void Get_1_RMost(char *);
void Get_3_Rmost(char *);
void Run_Bit_Manip();

#endif //C_PROGRAMMING_BITWISE_H
