#include "Bitwise.h"
#include "stdio.h"

char t_bin_1_1[] = "10101";
char t_bin_1_2[] = "00000";

char t_bin_2_1[] = "11111";
char t_bin_2_2[] = "11111";

char t_bin_3_1[] = "10101";
char t_bin_3_2[] = "11111";

char t_bin_4_1[] = "10101";
char t_bin_4_2[] = "00000";

char t_bin_5_1[] = "11111";
char t_bin_5_2[] = "11111";

char t_bin_6_1[] = "10101";
char t_bin_6_2[] = "11111";

char t_bin_7_1[] = "10101";
char t_bin_7_2[] = "00000";

char t_bin_8_1[] = "11111";
char t_bin_8_2[] = "11111";

char t_bin_9_1[] = "00000";
char t_bin_9_2[] = "11111";

char t_bin_10[] = "1";
char t_bin_11[] = "100";
char t_bin_12[] = "101";
char t_bin_13[] = "100";
char t_bin_14[] = "1010";
char t_bin_15[] = "1010";
char t_bin_16[] = "10101";
char t_bin_17[] = "11111";
char t_bin_18[] = "00000";

char t_bin_test1[] = "10101";
char t_bin_test2[] = "11111";
char t_bin_test3[] = "11100";

int Get_Dec(char binaryCharArray[]){

    char* start = &binaryCharArray[0];
    int total = 0;
    while (*start)
    {
        total *= 2;
        if (*start++ == '1') total += 1;
    }
    return total;
}

int Get_Bin(int num){

    if (num == 0) {
        return 0;
    }
    else {
        return (num % 2) + 10 * Get_Bin(num / 2);
    }
}

void Get_1_RMost(char *num){
    int b_num = (Get_Dec(num));
    b_num = b_num & (1 << 1);
    printf("\nRight Most Num For %s: %i", num, Get_Bin(b_num));
}

void Get_3_RMost(char * num){
    int b_num = (Get_Dec(num));
    b_num = b_num & (1 << 3);
    printf("\nRight Most Num For %s: %i", num, Get_Bin(b_num));
}

void Run_Bit_Manip(){

    //"&" Operations
    int b_1 = (Get_Dec(t_bin_1_1) & Get_Dec(t_bin_1_2));
    printf("\nProblem 1: %i", Get_Bin(b_1));

    int b_2 = (Get_Dec(t_bin_2_1) & Get_Dec(t_bin_2_2));
    printf("\nProblem 2: %i", Get_Bin(b_2));

    int b_3 = (Get_Dec(t_bin_3_1) & Get_Dec(t_bin_3_2));
    printf("\nProblem 3: %i", Get_Bin(b_3));

    //"|" Operations
    int b_4 = (Get_Dec(t_bin_4_1) & Get_Dec(t_bin_4_2));
    printf("\nProblem 4: %i", Get_Bin(b_4));

    int b_5 = (Get_Dec(t_bin_5_1) & Get_Dec(t_bin_5_2));
    printf("\nProblem 5: %i", Get_Bin(b_5));

    int b_6 = (Get_Dec(t_bin_6_1) & Get_Dec(t_bin_6_2));
    printf("\nProblem 6: %i", Get_Bin(b_6));

    //"^" Operations
    int b_7 = (Get_Dec(t_bin_7_1) & Get_Dec(t_bin_7_2));
    printf("\nProblem 7: %i", Get_Bin(b_7));

    int b_8 = (Get_Dec(t_bin_8_1) & Get_Dec(t_bin_8_2));
    printf("\nProblem 8: %i", Get_Bin(b_8));

    int b_9 = (Get_Dec(t_bin_9_1) & Get_Dec(t_bin_9_2));
    printf("\nProblem 9: %i", Get_Bin(b_9));

    //Shift Operations
    int b_10 = (Get_Dec(t_bin_10) << 3);
    printf("\nProblem 10: %i", Get_Bin(b_10));

    int b_11 = (Get_Dec(t_bin_11) << 1);
    printf("\nProblem 11: %i", Get_Bin(b_11));
    
    int b_12 = (Get_Dec(t_bin_12) << 2);
    printf("\nProblem 12: %i", Get_Bin(b_12));

    int b_13 = (Get_Dec(t_bin_13) >> 2);
    printf("\nProblem 13: %i", Get_Bin(b_13));

    int b_14 = (Get_Dec(t_bin_14) >> 1);
    printf("\nProblem 14: %i", Get_Bin(b_14));

    int b_15 = (Get_Dec(t_bin_15) >> 3);
    printf("\nProblem 15: %i", Get_Bin(b_15));

    int b_16 = (~Get_Dec(t_bin_16));
    printf("\nProblem 16: %i", Get_Bin(b_16));

    int b_17 = (~Get_Dec(t_bin_17));
    printf("\nProblem 17: %i", Get_Bin(b_17));

    int b_18 = (~Get_Dec(t_bin_18));
    printf("\nProblem 18: %i", Get_Bin(b_18));

    printf("\nProblem 19: ");
    Get_1_RMost(t_bin_test1);
    Get_1_RMost(t_bin_test2);
    Get_1_RMost(t_bin_test3);

    printf("\nProblem 20: ");
    Get_3_RMost(t_bin_test1);
    Get_3_RMost(t_bin_test2);
    Get_3_RMost(t_bin_test3);
}


