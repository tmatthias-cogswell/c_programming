#ifndef C_PROGRAMMING_FIVEFUNC_H
#define C_PROGRAMMING_FIVEFUNC_H

#include <stdio.h>
#include <stdarg.h>

void func_1(int);
void func_2(float, int);
void func_3(char*, char*, int);
void func_4();
void func_5(char*, ...);
void Assign_FiveFunc();

#endif //C_PROGRAMMING_FIVEFUNC_H
