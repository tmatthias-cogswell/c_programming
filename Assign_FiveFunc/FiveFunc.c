#include "FiveFunc.h"

void func_1(int a){
    printf("func_1 Output:  %d\n", a);
    return;
}

void func_2(float a, int b){
    printf("func_2 Output:  %f , %d\n", a, b);
    return;
}

void func_3(char* a, char* b, int c){
    printf("func_3 Output: %s, %s, %d\n", a, b, c);
    return;
}

void func_4(){
    printf("func_4 Output: \n");
    return;
}

void func_5(char* fmt, ...){
    const char *p;
    va_list argp;
    int i;
    char *s;
    char fmtbuf[256];

    va_start(argp, fmt);

    for(p = fmt; *p != '\0'; p++){
        if(*p != '%'){
            putchar(*p);
            continue;
        }

        switch(*++p){
            case 'c':
                i = va_arg(argp, int);
                putchar(i);
                break;

            case 'd':
                i = va_arg(argp, int);
                //s = vsprintf(fmt, argp);
                fputs(s, stdout);
                break;

            case 's':
                s = va_arg(argp, char *);
                fputs(s, stdout);
                break;

            case 'x':
                i = va_arg(argp, int);
                //s = vsprintf(fmt, argp);
                fputs(s, stdout);
                break;

            case '%':
                putchar('%');
                break;
        }
    }
    va_end(argp);
    return;
}

void Assign_FiveFunc(){
    func_1(1);
    func_2(2.3, 4);
    func_3("Test it!", "Test it again!", 5);
    func_4();
    func_5("Hello %s. Your age is %d\n", "Tom", 26);
}