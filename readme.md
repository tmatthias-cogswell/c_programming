## C Programming Assignments Collection ##

A collection of the assignments completed for SWE 110 - C Programming.

By navigating to the [source](https://bitbucket.org/tmatthias-cogswell/c_programming/src) section (located on the menu to
the left), each assignment can be viewed in its respective folder.

The project is built using CMake, and a CMakeLists.txt file is provided.  Also included is a pre-compiled binary
in bin/Release.

