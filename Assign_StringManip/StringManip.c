#include "StringManip.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void StringManip_1() {
    char str_1[128];
    char str_2[sizeof(str_1)];
    printf("\nInput Test String: ");
    fgets(str_1, sizeof(str_1), stdin);
    strcpy(str_2, str_1);
    printf("\nCopied String: %s", str_2);
}

void StringManip_2() {
    char str_1[128];
    char str_2[sizeof(str_1)];

    printf("Input Test String 1: ");
    fgets(str_1, sizeof(str_1), stdin);
    printf("Input Test String 2: ");
    fgets(str_2, sizeof(str_2), stdin);

    int cmp = strcmp(str_1, str_2);

    if (cmp < 0) {
        printf("str_1 is less than str_2");
    }
    else if (cmp > 0) {
        printf("str_2 is less than str_1");
    }
    else {
        printf("str_1 is equal to str_2");
    }
}

void StringManip_3() {
    char str_1[128];
    char str_2[sizeof(str_1)];

    printf("\nInput Test String 1: ");
    fgets(str_1, sizeof(str_1), stdin);
    printf("\nInput Test String 2: ");
    fgets(str_2, sizeof(str_2), stdin);

    if (strstr(str_1, str_2)) {

        char *search = strstr(str_1, str_2);
        printf("%c", *search);
        printf("\nstr_2 in str_1!");

    }
}

void StringManip_4() {
    char str_1[128];

    printf("\nInput Test String 1: ");
    fgets(str_1, sizeof(str_1), stdin);

    char *tokens = strtok(str_1, " ");

    while (tokens != NULL) {
        printf(" %s\n", tokens);
        tokens = strtok(NULL, " ");
    }
}

void StringManip_5() {
    char str_1[512];
    char str_2[512];
    printf("\nInput Test String 1: ");
    fgets(str_1, sizeof(str_1), stdin);
    char *tokens = strtok(str_1, " ");

    while (tokens != NULL) {
        char *temp = strdup(str_2);
        strcpy(str_2, tokens);
        strcat(str_2, " ");
        strcat(str_2, temp);
        tokens = strtok(NULL, " \n");
        free(temp);
    }
    printf("\nWords Reversed String: %s", str_2);
}

void StringManip_FileIn() {
    char str_1[1024];
    char str_2[1024];
    fgets(str_1, sizeof(str_1), stdin);
    char *tokens = strtok(str_1, " \n");

    while (tokens != NULL) {
        char *temp = strdup(str_2);
        strcpy(str_2, tokens);
        strcat(str_2, " ");
        strcat(str_2, temp);
        tokens = strtok(NULL, " \n");
        free(temp);
    }
    printf("\nWords Reversed String: %s", str_2);
}

void StringManip_All(){
    StringManip_1();
    StringManip_2();
    StringManip_3();
    StringManip_4();
    StringManip_5();
}



