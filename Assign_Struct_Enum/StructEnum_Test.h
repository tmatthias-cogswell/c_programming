#ifndef C_PROGRAMMING_STRUCTENUM_TEST_H
#define C_PROGRAMMING_STRUCTENUM_TEST_H

enum Class_Status {
    Freshman, Sophomore, Junior, Senior
};


typedef struct {
    unsigned long S_id;
    char First_Name[128];
    char Middle_Initial[128];
    char Last_Name[128];
    enum Class_Status Student_Status;
} Student;

typedef enum {
    false, true
} bool;
enum lem_type {
    Walker, Blocker, Floater, Miner, Builder
};

typedef struct {
    int lemNum;
    bool alive;
    enum lem_type lemType;
} Lemming;

//Student *new_student();
void print_Student(Student *);

void print_Lemming(Lemming *);

void student_test_run();

void lemTest();

void trimnl(char *);

#endif //C_PROGRAMMING_STRUCTENUM_TEST_H
