#include "StructEnum_Test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Student *new_student() {
    Student *newStudent = malloc(sizeof(Student));
    int id_buf = (int) newStudent;
    id_buf = abs(id_buf);
    printf("\nInput Num: %i", id_buf);
    newStudent->S_id = id_buf;
    printf("\nNew Student First Name: ");
    fgets(newStudent->First_Name, sizeof(newStudent->First_Name), stdin);
    trimnl(newStudent->First_Name);
    printf("\nNew Student Middle Initial: ");
    fgets(newStudent->Middle_Initial, sizeof(newStudent->Middle_Initial), stdin);
    trimnl(newStudent->Middle_Initial);
    printf("\nNew Student Last Name: ");
    fgets(newStudent->Last_Name, sizeof(newStudent->Last_Name), stdin);
    trimnl(newStudent->Last_Name);
    return newStudent;
}

void print_Student(Student *tempStudent) {
    char *Status_Array[] = {"Freshman", "Sophomore", "Junior", "Senior"};
    printf("\nStudent ID: %i", tempStudent->S_id);
    printf("\nName: %s %s %s", tempStudent->First_Name, tempStudent->Middle_Initial, tempStudent->Last_Name);
    printf("\nClass Status: %s", Status_Array[tempStudent->Student_Status]);
}

void student_test_run() {
    Student *testStudent = new_student();
    Student *testStudent_2 = new_student();
    print_Student(testStudent);
    print_Student(testStudent_2);

}

Lemming *new_Lemming() {
    Lemming *newLemming = malloc(sizeof(Lemming));
    int choice;
    printf("\nLemming Type?: ");
    printf("\n---------------");
    printf("\n1. Walker\n2. Blocker\n3. Floater\n4. Miner\n5. Builder");
    scanf("%d", &choice);
    choice--;
    switch (choice) {
        case 0:
            newLemming->lemType = 0;
            break;
        case 1:
            newLemming->lemType = 1;
            break;
        case 2:
            newLemming->lemType = 2;
            break;
        case 3:
            newLemming->lemType = 3;
            break;
        case 4:
            newLemming->lemType = 4;

    }
    newLemming->alive = true;
}

void print_Lemming(Lemming *tempLemming) {
    char *typeArray[] = {"Walker", "Blocker", "Floater", "Miner", "Builder"};
    printf("\nLemming Type:  %s", typeArray[tempLemming->lemType]);
}

void lemTest() {
    Lemming *testLem_1 = new_Lemming();
    Lemming *testLem_2 = new_Lemming();
    print_Lemming(testLem_1);
    print_Lemming(testLem_2);
}

void trimnl(char *s) {
    char *pnl = s + strlen(s);
    if (*s && *--pnl == '\n')
        *pnl = 0;
}
